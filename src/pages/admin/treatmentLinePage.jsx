import { useEffect, useState } from "react"
import treatmentLineService from "../../services/treatmentLineService"
import Navbar from "../../component/navbar"

function TreatmentLinePage() {
    const [treatmentLineDatas, setTreatmentLineDatas] = useState([])

    const loadTreatmentLineDatas = () => {
        treatmentLineService.findAll()
            .then(res => setTreatmentLineDatas(res.data))
            .catch(err => {
                throw new Error(err)
            })
    }

    useEffect(() => {
        const hasSessionData = sessionStorage.getItem('JhhyAn148G7482GT@T')
        if (!hasSessionData) {
            window.location.href = '/'
        } else {
            loadTreatmentLineDatas()
        }
    }, [])

    return <>

        <div className="container-body">
            <Navbar />
            <h3 className="text-start mb-3">Queue</h3>
            <table className="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Treatment</th>
                        <th>Patient</th>
                        <th>State</th>
                    </tr>
                </thead>
                <tbody>

                    {
                        Array.isArray(treatmentLineDatas) ? (
                            treatmentLineDatas.map((treatmentLine, index) => (
                                <tr key={index}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{treatmentLine.date}</td>
                                    <td>{treatmentLine.treatment.name}</td>
                                    <td>{treatmentLine.patientName}</td>
                                    <td>{treatmentLine.state}</td>
                                </tr>
                            ))
                        ) : ("")
                    }

                </tbody>
            </table>
        </div>
    </>


}
export default TreatmentLinePage