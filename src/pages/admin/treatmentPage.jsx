import { useEffect, useState } from "react"
import categoryService from "../../services/categoryService"
import UpdateTreatmentModal from "../../component/updateTreatmentModal"
import NewTreatmentModal from "../../component/newTreatmentModal"
import Navbar from "../../component/navbar"

function TreatmentPage() {
    const [category, setCategory] = useState("diagnostic")
    const [treatmentDatas, setTreatmentDatas] = useState([])
    const [treatmentSelected, setTreatmentSelected] = useState({})


    const loadCategoryDatas = () => {
        categoryService.findAll()
            .then(res => {
                if (category === "diagnostic") {
                    let treatments = res.data[0].treatments
                    setTreatmentDatas(treatments)
                } else if (category === "examens") {
                    let treatments = res.data[1].treatments
                    setTreatmentDatas(treatments)
                }
            })
            .catch(err => {
                throw new Error(err)
            })
    }

    useEffect(() => {
        const hasSessionData = sessionStorage.getItem('JhhyAn148G7482GT@T')
        if (!hasSessionData) {
            window.location.href = '/'
        } else {
            loadCategoryDatas()
        }
    }, [category])


    return <>
        <div className="container-body">
            <Navbar />
            <h3 className="text-start mb-3">Treatment</h3>
            <div className="mb-4 treatment-header">
                <div>
                    <label className="form-label">Filtered by category</label>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="options" id="diagnostic" value="diagnostic" checked={category === "diagnostic"} onChange={(e) => setCategory(e.target.value)} />
                        <label className="form-check-label" htmlFor="diagnostic">Diagnostic</label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="options" id="examens" value="examens" checked={category === "examens"} onChange={(e) => setCategory(e.target.value)} />
                        <label className="form-check-label" htmlFor="examens">Examens</label>
                    </div>
                </div>
                <div>
                    <button className="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#newTreatmentModal">New</button>
                </div>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Translation</th>
                        <th>State</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    {
                        Array.isArray(treatmentDatas) ? (
                            treatmentDatas.map((treatment, index) => (
                                <tr key={index}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{treatment.name}</td>
                                    <td>{treatment.traduction !== "" ? treatment.traduction : "no translation"}</td>
                                    <td>{treatment.state ? "available" : "unavailable"}</td>
                                    <td>
                                        <button onClick={() => setTreatmentSelected(treatment)} type="button" className="me-2" data-bs-toggle="modal" data-bs-target="#updateTreatmentModal">
                                            <i className="bi bi-pencil"></i>
                                        </button>
                                    </td>
                                </tr>
                            ))
                        ) : ("")
                    }

                </tbody>
            </table>
            <UpdateTreatmentModal treatmentSelected={treatmentSelected} />
            <NewTreatmentModal />
        </div>
    </>

}
export default TreatmentPage