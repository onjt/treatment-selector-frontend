import { useEffect, useState } from "react"
import treatmentLineService from "../../services/treatmentLineService"
import categoryService from "../../services/categoryService"

function PublicPage() {
    const [category, setCategory] = useState("diagnostic")
    const [treatmentDatas, setTreatmentDatas] = useState([])
    const [isTransleted, setIsTranslated] = useState(false)
    const [treatmentSelected, setTreatmentSelected] = useState("")
    const [patientName, setPatientName] = useState("")


    console.log("treatmentDatas >>", treatmentDatas)

    const loadCategoryDatas = () => {
        categoryService.findAll()
            .then(res => {
                if (category === "diagnostic") {
                    let treatments = res.data[0].treatments
                    setTreatmentDatas(treatments)
                } else if (category === "examens") {
                    let treatments = res.data[1].treatments
                    setTreatmentDatas(treatments)
                }
            })
            .catch(err => {
                throw new Error(err)
            })
    }

    useEffect(() => {
        loadCategoryDatas()
    }, [category])

    const treatmentLineObject = {
        "date": new Date().toLocaleString(),
        "patientName": patientName,
        "state": "pending"
    }

    const handleSubmit = async () => {
        try {
            await treatmentLineService.save(treatmentSelected, treatmentLineObject)
        } catch (error) {
            console.log("error ", error)
        }
        window.location.reload()
    }

    return <div className="container-body">
        <div className="header-section">
            <h2>Choose Your Treatment</h2>
            <p>
                Welcome to our state-of-the-art clinic, where we put the power in your hands to choose the treatment that best suits your individual health needs.
            </p>
        </div>

        <div className="category-section">
            <div className="mb-4">
                <label className="form-label">Choose a category</label>
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="options" id="diagnostic" value="diagnostic" checked={category === "diagnostic"} onChange={(e) => setCategory(e.target.value)} />
                    <label className="form-check-label" htmlFor="diagnostic">Diagnostic</label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="options" id="examens" value="examens" checked={category === "examens"} onChange={(e) => setCategory(e.target.value)} />
                    <label className="form-check-label" htmlFor="examens">Examens</label>
                </div>
            </div>
        </div>
        <div className="form-section">
            <div className="mb-4">
                <label htmlFor="treatmentSelect" className="form-label">Choose a treatment</label>
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" name="options" id="translate" checked={isTransleted} onChange={(e) => setIsTranslated(e.target.checked)} />
                    <label className="form-check-label" htmlFor="translate">Translate ?</label>
                </div>
                <select
                    className="form-select"
                    id="treatmentSelect"
                    value={treatmentSelected}
                    onChange={(e) => {
                        if (e.target.value !== "") {
                            setTreatmentSelected(e.target.value)
                        }
                    }}>
                    <option value={""}>choose your treatment</option>
                    {
                        Array.isArray(treatmentDatas) ? (
                            treatmentDatas.map(treatment => (
                                <option key={treatment.name} value={treatment.id}>
                                    {!isTransleted ?
                                        (treatment.name) :
                                        (treatment.traduction === "" ? treatment.name : treatment.traduction)
                                    }
                                </option>
                            ))
                        ) : ("")
                    }
                </select>
            </div>
            <div className="mb-4">
                <label htmlFor="userName" className="form-label">Enter your name</label>
                <input
                    type="text"
                    className="form-control"
                    id="userName"
                    placeholder="ex : Ramanitrarivo Onjatiana"
                    value={patientName}
                    onChange={(e) => setPatientName(e.target.value)} />
            </div>
            <div className="mt-4 text-center">
                <button type="button" className="btn btn-secondary" onClick={handleSubmit}>Confirm</button>
            </div>
        </div>
    </div>
}
export default PublicPage