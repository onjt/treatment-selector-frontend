import { BrowserRouter, Route, Routes } from "react-router-dom";
import TreatmentPage from "../pages/admin/treatmentPage";
import PublicPage from "../pages/public/publicPage";
import TreatmentLinePage from "../pages/admin/treatmentLinePage";
import SignInPage from "../pages/admin/signInPage";

function AppRoutes() {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<PublicPage />} />
                    <Route path='/admin' element={<SignInPage />} />
                    <Route path='/admin/treatment' element={<TreatmentPage />} />
                    <Route path='/admin/treatment-line' element={<TreatmentLinePage />} />
                </Routes>
            </BrowserRouter>
        </>

    )
}
export default AppRoutes