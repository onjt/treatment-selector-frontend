import axios from "axios"

const url = "http://localhost:8080/category"

const categoryService = {
    findAll: async () => {
        const datas = await axios.get(url + "/list")
        return datas
    }
}
export default categoryService