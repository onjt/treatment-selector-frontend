import axios from "axios"

const url = "http://localhost:8080/treatment-line"

const treatmentLineService = {
    findAll: async () => {
        return await axios.get(url + "/list")
    },
    save: async (id, obj) => {
        await axios.post(url + "/save/" + id, obj)
    }
}
export default treatmentLineService