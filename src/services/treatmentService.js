import axios from "axios"

const url = "http://localhost:8080/treatment"

const treatmentService = {
    save: async (id, obj) => {
        await axios.post(url + "/save/" + id, obj)
    },
    update: async (id, obj) => {
        await axios.put(url + "/update/" + id, obj)
    }
}
export default treatmentService